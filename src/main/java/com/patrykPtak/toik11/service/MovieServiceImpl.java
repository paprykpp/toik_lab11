package com.patrykPtak.toik11.service;

import com.patrykPtak.toik11.dto.MovieDto;
import com.patrykPtak.toik11.repository.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Map<String, List<MovieDto>> getMovieList() {
        List<MovieDto> movieDtoList = movieRepository.getMovies();
        Map<String, List<MovieDto>> mapMovies = new HashMap<>();
        mapMovies.put("movies", movieDtoList);
        return mapMovies;
    }
}
