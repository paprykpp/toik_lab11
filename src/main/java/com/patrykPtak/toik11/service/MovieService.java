package com.patrykPtak.toik11.service;

import com.patrykPtak.toik11.dto.MovieDto;
import java.util.List;
import java.util.Map;

public interface MovieService {
    Map<String, List<MovieDto>> getMovieList();
}
