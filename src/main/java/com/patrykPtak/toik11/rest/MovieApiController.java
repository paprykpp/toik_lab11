package com.patrykPtak.toik11.rest;

import com.patrykPtak.toik11.dto.MovieDto;
import com.patrykPtak.toik11.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

@RestController
public class MovieApiController {
    MovieService movieService;

    @Autowired
    public MovieApiController(MovieService movieService) {
        this.movieService = movieService;
    }

    @CrossOrigin
    @GetMapping(value = "/movies")
    public ResponseEntity<Map<String, List<MovieDto>>> allMovies(){
        return new ResponseEntity<>(movieService.getMovieList(), HttpStatus.OK);
    }

}