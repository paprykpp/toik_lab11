package com.patrykPtak.toik11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Toik11Application {

	public static void main(String[] args) {
		SpringApplication.run(Toik11Application.class, args);
	}

}
