package com.patrykPtak.toik11.repository;

import com.patrykPtak.toik11.dto.MovieDto;
import java.util.List;

public interface MovieRepository {
    List<MovieDto> getMovies();
}
